
local cmds = _mines.cmds

cmds.require_priv = function (name)
    if minetest.check_player_privs(name, {mines = true}) or minetest.check_player_privs(name, {server = true}) then
        return false
    end
    return true
end

cmds.create = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines create <mine_name>                 Creates a new mine with given mine name")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    -- Need to restrict it so it can't be any of the subcmds
    if _mines.tools.tableContainsValue(_mines.invalid_mine_names, mine_name) then -- This just stops list, what about the others
        minetest.chat_send_player(name, "Can't use '"..mine_name.."' as mine name, reserved for '/mines "..mine_name.."'.")
        return true
    end
    local rc = mines.create_mine(mine_name, name)
    if not rc.success then
        minetest.chat_send_player(name, rc.errmsg)
        _mines.tools.log("mines create " .. mine_name .. " => " .. rc.errmsg)
        return true
    end
    minetest.chat_send_player(name, "Mine '"..mine_name.."' created (additional setup required)")
    minetest.chat_send_player(name, "Use '/mines pos1 "..mine_name.."' to set position 1")
    minetest.chat_send_player(name, "Use '/mines pos2 "..mine_name.."' to set position 2")
    minetest.chat_send_player(name, "Use '/mines blocks "..mine_name.." add itemstring=percent' to add blocks")
    minetest.chat_send_player(name, "Use '/mines blocks "..mine_name.." add hand=percent' to add blocks (by just holding the item in your hand)")
    minetest.chat_send_player(name, "Use '/mines settp "..mine_name.."' to set a teleport point")
    minetest.chat_send_player(name, "Use '/mines settimer "..mine_name.." <seconds>' to set the mine to a time based regeneration")
    minetest.chat_send_player(name, "Use '/mines setpercent "..mine_name.." <percent>' to set the mine to a percent based regeneration")
    minetest.chat_send_player(name, "Use '/mines active "..mine_name.."' to enable the mine")
    return true
end

cmds.info = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "mines info <mine_name>     Obtains information on the given mine")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        minetest.chat_send_player(name, rc.errmsg)
        _mines.tools.log("mines info " .. mine_name .. " => " .. rc.errmsg)
        return true
    end
    local m = rc.val
    minetest.chat_send_player(name, "Mine '" .. mine_name .. "' Info:")
    minetest.chat_send_player(name, "  Node Count:     " .. tostring(#m.nodes))
    minetest.chat_send_player(name, "  Pos1:           " .. m.pos1)
    minetest.chat_send_player(name, "  Pos2:           " .. m.pos2)
    if m.active then
        minetest.chat_send_player(name, "  Active:         Yes")
    else
        minetest.chat_send_player(name, "  Active:         No")
    end
    local pos1 = _mines.tools.str2pos(m.pos1)
    local pos2 = _mines.tools.str2pos(m.pos2)
    local sizex = math.abs(pos1.x - pos2.x)
    local sizey = math.abs(pos1.y - pos2.y)
    local sizez = math.abs(pos1.z - pos2.z)
    local total = sizex * sizey * sizez
    minetest.chat_send_player(name, "  Size:           " .. tostring(sizex) .. "x" .. tostring(sizey) .. "x" .. tostring(sizez) .. " (" .. tostring(total) .. ")")
    -- Add code to show the percentage based on the 2 different regeneration types (time, percent)
    if m.regen_type == "time" then
        local percent = (((m.regen-m.timer) / m.regen) * 100.0)
        percent = string.format("%0.1f", percent)
        minetest.chat_send_player(name, "  Regen: (TIME)   " .. tostring(m.timer) .. "/" .. tostring(m.regen) .. " = " .. percent .. "%")
    elseif m.regen_type == "perc" then
        local fill = _mines.tools.filled(pos1, pos2)
        local percent = ((fill/total) * 100.0)
        percent = string.format("%0.1f", percent)
        minetest.chat_send_player(name, "  Regen: (PERC)  " .. tostring(fill) .. "/" .. tostring(total) .. " = " .. percent .. "%")
    end
    -- Only show teleport point if the player can teleport there
    if not m.public and (_mines.tools.tableContainsValue(m.privs, name) or _mines.tools.tableContainsValue(m.mods, name) or m.owner == name) then
        if m.tp ~= "" then
            minetest.chat_send_player(name, "  Teleport Point: " .. m.tp)
        else
            minetest.chat_send_player(name, "  Teleport Point: None Set")
        end
    else
        if m.tp ~= "" then
            minetest.chat_send_player(name, "  Teleport Point: Not accessable")
        else
            minetest.chat_send_player(name, "  Teleport Point: None Set")
        end
    end
    -- Show extra things like priv level if they are the owner or a moderator (don't show to regulars)
    if m.owner == name then
        minetest.chat_send_player(name, "  Privs:          Owner")
    elseif _mines.tools.tableContainsValue(m.mods, name) then
        minetest.chat_send_player(name, "  Privs:          Moderator")
    end
    return true
end

cmds.help = function (name)
    minetest.chat_send_player(name, "Mines Help:")
    minetest.chat_send_player(name, "  mines help (category)                    Displays help (Can be optionally given a subcmd)")
    minetest.chat_send_player(name, "  mines info <mine_name>                   Obtains information on the given mine")
    minetest.chat_send_player(name, "  mines pos1 <mine_name>                   Sets position 1 for given mine")
    minetest.chat_send_player(name, "  mines pos2 <mine_name>                   Sets position 2 for given mine")
    minetest.chat_send_player(name, "  mines create <mine_name>                 Creates a new mine with given mine name")
    minetest.chat_send_player(name, "  mines delete <mine_name>                 Deletes the given mine, blocks of that mine will remain")
    minetest.chat_send_player(name, "  mines clear <mine_name>                  Clears all the blocks within the given mine")
    minetest.chat_send_player(name, "  mines show <mine_name>                   Displays the area the given mine will take")
    minetest.chat_send_player(name, "  mines settp <mine_name>                  Assigns the Teleport Point which is used for mine regeneration and teleportation")
    minetest.chat_send_player(name, "  mines settimer <mine_name> <seconds>     Number of seconds till this mine regenerates")
    minetest.chat_send_player(name, "  mines setpercent <mine_name> <percent>   Percent left of the mine till it regenerates (Sets to percent based regeneration)")
    minetest.chat_send_player(name, "  mines active <mine_name>                 Setting the given mine to active")
    minetest.chat_send_player(name, "  mines inactive <mine_name>               Setting the given mine to inactive")
    minetest.chat_send_player(name, "  mines toggle <mine_name>                 Toggles the given mine active or inactive")
    minetest.chat_send_player(name, "  mines regen <mine_name>                  Forces the mine to regenerate")
    minetest.chat_send_player(name, "  mines reset <mine_name>                  Forces the mine to regenerate")
    minetest.chat_send_player(name, "  mines list                               Lists all mines created")
    minetest.chat_send_player(name, "  mines <mine_name>                        Teleports to the given mine name (Can fail if the mine doesn't have a Teleport Point set or you don't have permission via privs subcmd)")
    minetest.chat_send_player(name, "  mines help blocks                        Shows the blocks subcmd help")
    minetest.chat_send_player(name, "  mines help privs                         Shows the privs subcmd help")
    minetest.chat_send_player(name, "  mines help moderator                     Shows the moderator subcmd help (can be abreviated mod instead of moderator)")
    -- minetest.chat_send_player(name, "")
end

cmds.pos1 = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines pos1 <mine_name>                   Sets position 1 for given mine")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        minetest.chat_send_player(name, rc.errmsg)
        _mines.tools.log("mines pos1 " .. mine_name .. " => " .. rc.errmsg)
        return true
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        minetest.chat_send_player(name, "You can't modify '"..mine_name.."'")
        return true
    end
    local player = minetest.get_player_by_name(name) or nil
    if player == nil then
        minetest.chat_send_player(name, "You don't exist?")
        _mines.tools.log("mines pos1 " .. mine_name .. " => Player '" .. name .. "' doesn't exist (is nil)")
        return true
    end
    local pos1 = _mines.tools.pos2str(player:get_pos())
    mi.pos1 = pos1
    mines.save_mine(mine_name, mi)
    minetest.chat_send_player(name, "Set position 1 at (" .. pos1 .. ")")
    return true
end

cmds.pos2 = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines pos2 <mine_name>                   Sets position 2 for given mine")
        return
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        minetest.chat_send_player(name, rc.errmsg)
        _mines.tools.log("mines pos2 " .. mine_name .. " => " .. rc.errmsg)
        return true
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        minetest.chat_send_player(name, "You can't modify '"..mine_name.."'")
        return true
    end
    local player = minetest.get_player_by_name(name) or nil
    if player == nil then
        minetest.chat_send_player(name, "You don't exist?")
        _mines.tools.log("mines pos2 " .. mine_name .. " => Player '" .. name .. "' doesn't exist (is nil)")
        return true
    end
    local pos2 = _mines.tools.pos2str(player:get_pos())
    mi.pos2 = pos2
    mines.save_mine(mine_name, mi)
    minetest.chat_send_player(name, "Set position 2 at (" .. pos2 .. ")")
    return true
end

cmds.list = function (name, args)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    m = _mines.tools.tableKeys(m)
    -- _mines.tools.log(minetest.serialize(m) .. " " .. #m)
    if #m == 0 then
        return true, "There are no mines. :("
    end
    minetest.chat_send_player(name, "There are " .. #m .. " mines:")
    for idx, mine_name in ipairs(m) do
        local rc = mines.info_mine(mine_name)
        if not rc.success then
            _mines.tools.log("mines list " .. mine_name .. " => " .. rc.errmsg)
            return false, rc.errmsg
        end
        local mi = rc.val
        if not mi.public and (_mines.tools.tableContainsValue(mi.privs, name) or _mines.tools.tableContainsValue(mi.mods, name) or mi.owner == name) then
            minetest.chat_send_player(name, " " .. mine_name)
        elseif mi.public then
            minetest.chat_send_player(name, " " .. mine_name)
        end
    end
    return true
end

cmds.delete = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines delete <mine_name>                 Deletes the given mine, blocks of that mine will remain")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    if not mines.is_mine(mine_name) then
        return true, "Can't delete mine '" .. mine_name .. "', it doesn't exist."
    end
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines delete " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name then
        return true, "Can't delete mine '" .. mine_name .. "', must be owner."
    end
    rc = mines.remove_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines delete " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    return true, "Mine '" .. mine_name .. "' deleted."
end

cmds.show = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines show <mine_name>                   Displays the area the given mine will take")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    if not mines.is_mine(mine_name) then
        return true, "Can't show mine '" .. mine_name .. "', it doesn't exist."
    end
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines show " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    rc = mines.fill_mine(mine_name, "mines:show")
    if not rc.success then
        _mines.tools.log("mines show " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    minetest.after(60, function ()
        local rc = mines.fill_mine(mine_name, "air")
        if not rc.success then
            _mines.tools.log("mines show (after 60) " .. mine_name .. " => " .. rc.errmsg)
        end
    end)
    return true
end

cmds.clear = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines clear <mine_name>                  Clears all the blocks within the given mine")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    if not mines.is_mine(mine_name) then
        return true, "Can't clear mine '" .. mine_name .. "', it doesn't exist."
    end
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines show " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    rc = mines.fill_mine(mine_name, "air")
    if not rc.success then
        _mines.tools.log("mines show " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    return true
end

cmds.reset = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines regen <mine_name>                  Forces the mine to regenerate")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines regen " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    rc = mines.regen_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines regen " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    return true
end

cmds.settp = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines settp <mine_name>                  Assigns the Teleport Point which is used for mine regeneration and teleportation")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local reset = #args >= 2
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines settp " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    if not reset then
        local player = minetest.get_player_by_name(name) or nil
        if player == nil then
            minetest.chat_send_player(name, "You don't exist?")
            _mines.tools.log("mines settp " .. mine_name .. " => Player '" .. name .. "' doesn't exist (is nil)")
            return true
        end
        local pos = _mines.tools.pos2str(player:get_pos())
        mi.tp = pos
        rc = mines.save_mine(mine_name, mi)
        if not rc.success then
            _mines.tools.log("mines settp " .. mine_name .. " => " .. rc.errmsg)
            return false, rc.errmsg
        end
        return true, "Set mine '" .. mine_name .. "' teleport point to " .. pos
    else
        mi.tp = ""
        rc = mines.save_mine(mine_name, mi)
        if not rc.success then
            _mines.tools.log("mines settp " .. mine_name .. " => " .. rc.errmsg)
            return false, rc.errmsg
        end
        return true, "Cleared mine '" .. mine_name .. "' teleport point."
    end
end

cmds.settimer = function (name, args)
    if #args == 0 or #args < 2 then
        minetest.chat_send_player(name, "  mines settimer <mine_name> <seconds>     Number of seconds till this mine regenerates (Sets to time based regeneration)")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local time = tonumber(args[2])
    if time == "fail" then
        minetest.chat_send_player(name, "  mines settimer <mine_name> <seconds>     Number of seconds till this mine regenerates (Sets to time based regeneration)")
        minetest.chat_send_player(name, "Invalid seconds parameter. (must be a number)")
        return true
    end
    if time < 0 then -- Can not be negative
        time = -time
    end
    if time < 1 then -- Can not be less than 1.0 seconds
        time = 1
    end
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines settimer " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    mi.regen_type = "time"
    mi.regen = time
    mi.timer = mi.regen
    rc = mines.save_mine(mine_name, mi)
    if not rc.success then
        _mines.tools.log("mines settimer " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    return true, "Set " .. mine_name .. " to regenerate every " .. time .. " seconds."
end

cmds.setpercent = function (name, args)
    if #args == 0 or #args < 2 then
        minetest.chat_send_player(name, "  mines setpercent <mine_name> <percent>   Percent left of the mine till it regenerates (Sets to percent based regeneration)")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local time = tonumber(args[2])
    if time == "fail" then
        minetest.chat_send_player(name, "  mines setpercent <mine_name> <percent>   Percent left of the mine till it regenerates (Sets to percent based regeneration)")
        minetest.chat_send_player(name, "Invalid seconds parameter. (must be a number, between 0 and 100)")
        return true
    end
    if time < 0 then -- Can not be negative
        time = -time
    end
    if time < 0 then -- Can not be less than 0
        time = 0
    end
    if time > 100 then -- Can not be greater than 100
        time = 100
    end
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines setpercent " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    mi.regen_type = "perc"
    mi.regen = time
    mi.timer = 0
    rc = mines.save_mine(mine_name, mi)
    if not rc.success then
        _mines.tools.log("mines setpercent " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    return true, "Set " .. mine_name .. " to regenerate once it is mined below " .. time .. "%"
end

cmds.active = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines active <mine_name>                 Setting the given mine to active")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines active " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    mi.active = true
    rc = mines.save_mine(mine_name, mi)
    if not rc.success then
        _mines.tools.log("mines active " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    return true, "Mine " .. mine_name .. " is now active."
end

cmds.inactive = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines inactive <mine_name>               Setting the given mine to inactive")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines inactive " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    mi.active = false
    rc = mines.save_mine(mine_name, mi)
    if not rc.success then
        _mines.tools.log("mines inactive " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    return true, "Mine " .. mine_name .. " is now inactive."
end

cmds.toggle = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines toggle <mine_name>                 Toggles the given mine active or inactive")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines toggle " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    mi.active = not mi.active
    rc = mines.save_mine(mine_name, mi)
    if not rc.success then
        _mines.tools.log("mines toggle " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    if mi.active then
        return true, "Mine " .. mine_name .. " is now active. (Toggled)"
    else
        return true, "Mine " .. mine_name .. " is now inactive. (Toggled)"
    end
end

cmds.block = {}
local block = cmds.block

block.list = function (name, args)
    if #args == 0 then
        minetest.chat_send_player(name, "  mines blocks <mine_name>                             Lists all blocks and their percentages for the given mine")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines blocks " .. mine_name .. " => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    minetest.chat_send_player(name, "Mine '" .. mine_name .. "' has " .. #mi.nodes .. " blocks:")
    for _, b in ipairs(mi.nodes) do
        local tmp = _mines.tools.split(b, "=")
        minetest.chat_send_player(name, "  " .. tmp[1] .. " " .. tmp[2] .. "%")
    end
    local total = _mines.tools.addPercent(mi.nodes)
    if total < 100 then
        local left = 100 - total
        minetest.chat_send_player(name, "There is " .. left .. "% left.")
    end
    return true
end

block.add = function (name, args)
    if #args == 0 or #args < 3 then
        minetest.chat_send_player(name, "  mines blocks <mine_name> add itemstring=percent      Adds a block to given mine, given itemstring and percent (minimum 0.1, maximum 100)")
        minetest.chat_send_player(name, "   DON'T forget, you can use 'hand=percent' instead of giving 'itemstring=percent'!")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines blocks " .. mine_name .. " add => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    -- args[2] == add (ignore that part)
    local work = args[3]
    local parts = _mines.tools.split(work, "=")
    if #parts ~= 2 then
        minetest.chat_send_player(name, "  mines blocks <mine_name> add itemstring=percent      Adds a block to given mine, given itemstring and percent (minimum 0.1, maximum 100)")
        minetest.chat_send_player(name, "itemstring=percent was missing. (e.g. 'default:stone_with_coal=50' for 50% Coal Ore)")
        minetest.chat_send_player(name, "DON'T forget, you can use 'hand=percent' instead of giving 'itemstring=percent'!")
        return true
    end
    local item = parts[1]:lower()
    local perc = tonumber(parts[2]:lower())
    if item == "hand" then
        local player = minetest.get_player_by_name(name) or nil
        if player == nil then
            minetest.chat_send_player(name, "You don't exist?")
            _mines.tools.log("mines blocks " .. mine_name .. " add => Player '" .. name .. "' doesn't exist (is nil)")
            return true
        end
        --_mines.tools.log(minetest.serialize(player:get_wielded_item():to_table()))
        item = player:get_wielded_item() or ""
        if item == "" then
            return false, "You need something in your hand to use."
        end
        item = item:get_name()
        if minetest.registered_nodes[item] == nil then
            return false, "Invalid, item is not node. (It must be a placeable item)"
        end
    end
    if perc == "fail" then
        return false, "Invalid, percent is not a number. (Must be a number within 0.1 and 100)"
    end
    if perc < 0 then
        return false, "Invalid, percent is out of bounds. (Too low, minimum 0.1)"
    end
    if perc > 100 then
        return false, "Invalid, percent is out of bounds. (Too high, maximum 100)"
    end
    local rc = mines.add_node(mine_name, item, perc)
    if not rc.success then
        _mines.tools.log("mines blocks " .. mine_name .. " add => " .. rc.errmsg)
        return false, rc.errmsg
    end
    minetest.chat_send_player(name, item .. " " .. perc .. "% added")
    rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines blocks " .. mine_name .. " add => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local total = _mines.tools.addPercent(rc.val.nodes)
    if total < 100 then
        local left = 100 - total
        minetest.chat_send_player(name, "There is " .. left .. "% left.")
    end
    return true
end

block.rm = function (name, args)
    if #args == 0 or #args < 3 then
        minetest.chat_send_player(name, "  mines blocks <mine_name> rm itemstring               Removes a block from the given mine, given itemstring")
        minetest.chat_send_player(name, "   DON'T forget, you can use 'hand' instead of giving 'itemstring'!")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines blocks " .. mine_name .. " rm => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    -- args[2] == add (ignore that part)
    local item = args[3]:lower()
    if item == "hand" then
        local player = minetest.get_player_by_name(name) or nil
        if player == nil then
            minetest.chat_send_player(name, "You don't exist?")
            _mines.tools.log("mines blocks " .. mine_name .. " rm => Player '" .. name .. "' doesn't exist (is nil)")
            return true
        end
        --_mines.tools.log(minetest.serialize(player:get_wielded_item():to_table()))
        item = player:get_wielded_item() or ""
        if item == "" then
            return false, "You need something in your hand to use."
        end
        item = item:get_name()
        if minetest.registered_nodes[item] == nil then
            return false, "Invalid, item is not node. (It must be a placeable item)"
        end
    end
    local rc = mines.remove_node(mine_name, item)
    if not rc.success then
        _mines.tools.log("mines blocks " .. mine_name .. " rm => " .. rc.errmsg)
        return false, rc.errmsg
    end
    minetest.chat_send_player(name, item .. " removed")
    rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines blocks " .. mine_name .. " rm => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local total = _mines.tools.addPercent(rc.val.nodes)
    if total < 100 then
        local left = 100 - total
        minetest.chat_send_player(name, "There is " .. left .. "% left.")
    end
    return true
end

block.clear = function (name, args)
    if #args == 0 or #args < 2 then
        minetest.chat_send_player(name, "  mines blocks <mine_name> clear                       Clears all blocks from the given mine")
        return true
    end
    if cmds.require_priv(name) then
        minetest.chat_send_player(name, "Insufficient privilege")
        return true
    end
    local mine_name = args[1]
    local rc = mines.info_mine(mine_name)
    if not rc.success then
        _mines.tools.log("mines blocks " .. mine_name .. " clear => " .. rc.errmsg)
        return false, rc.errmsg
    end
    local mi = rc.val
    if mi.owner ~= name and not _mines.tools.tableContainsValue(mi.mods, name) then
        return false, "Action not allowed on mine '" .. mine_name .. "'."
    end
    local count = #mi.nodes
    mi.nodes = {}
    rc = mines.save_mine(mine_name, mi)
    if not rc.success then
        _mines.tools.log("mines blocks " .. mine_name .. " clear => " .. rc.errmsg)
        return false, rc.errmsg
    end
    return true, "All " .. count .. " blocks removed from mine '" .. mine_name .. "'."
end
