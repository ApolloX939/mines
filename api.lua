
local api = mines

api.is_mine = function (mine_name)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    return m[mine_name] ~= nil
end

-- Creates a mine given it's name and owner's name
api.create_mine = function (mine_name, player_name)
    if api.is_mine(mine_name) then
        return {success=false, errmsg="Mine '"..mine_name.."' already exists, edit not create", val=nil}
    end
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    m[mine_name] = { -- older versions will be incompatable with newer versions (try to build a converter when able)
        nodes= {}, -- "node:name=percent", ...
        pos1= "0 0 0", -- Point 1
        pos2= "0 0 0", -- Point 2
        active= false, -- Enable or disable the mine, incases where it's time based
        regen_type= "time", -- If the mine regenerates based on time, or percent
        regen= 60, -- How many "ticks" till this mine regenerates (or percent till the mine should be regenerated)
        timer= 0, -- Current time (Set to regen when 0 or below 0, then spawn in terrain, will be left 0 if mine regen_type isn't "time")
        tp= "", -- If defined, where to place players who were in the mine while it was about to regenerate
        owner= player_name, -- The main owner of the mine (prevents mods from removing other mods and or delete all other mods)
        mods= {}, -- who can change the nodes, positions, active, change regen, regen_type, force a regeneration (can't change owner)
        privs= {}, -- Who can teleport to this mine (mods and owner can always teleport to the mine)
        public= false, -- If the privs list is ignored (true) or used (false)
    }
    _mines.store:set_string("mines", minetest.serialize(m))
    return {success=true, errmsg="", val=nil}
end

api.make_genlist = function (mine_name)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    if not api.is_mine(mine_name) then
        return {success=false, errmsg="Mine '"..mine_name.."' doesn't exist", val=nil}
    end
    local mi = m[mine_name]
    local list = {}
    if #mi.nodes == 0 then
        if _mines.GAMEMODE == "MTG" then
            return {success=true, errmsg="", val={"default:stone"}}
        elseif _mines.GAMEMODE == "MCL2" or _mines.GAMEMODE == "MCL5" then
            return {success=true, errmsg="", val={"mcl_core:stone"}}
        else
            return {success=true, errmsg="", val={"air"}}
        end
    elseif #mi.nodes == 1 then
        local v = _mines.tools.split(mi.nodes[1], "=")
        -- Replace unsafe uranium for safe uranium
        if v[1] == "technic:mineral_uranium" then
            v[1] = "mines:uranium_ore"
        end
        return {success=true, errmsg="", val={v[1]}}
    end
    for k, v in pairs(mi.nodes) do
        local v = _mines.tools.split(v, "=")
        if v[#v] == 100 then
            if v[1] == "technic:mineral_uranium" then
                v[1] = "mines:uranium_ore"
            end
            return {success=true, errmsg="", val={v[1]}}
        end
        if v[#v] ~= 0 then
            if v[1] == "technic:mineral_uranium" then
                v[1] = "mines:uranium_ore"
            end
            for i=0.0, tonumber(v[#v]) or 0.1, 0.1 do
                table.insert(list, v[1])
            end
        end
    end
    return {success=true, errmsg="", val=list}
end

-- Removes a mine and ALL it's data, just given the mine's name
api.remove_mine = function (mine_name)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    if api.is_mine(mine_name) then
        m[mine_name] = nil
        local idx = 1
        for ind, val in pairs(m) do
            if ind == mine_name then
                break
            end
            idx = idx + 1
        end
        table.remove(m, idx)
        _mines.store:set_string("mines", minetest.serialize(m))
        return {success=true, errmsg="", val=nil}
    end
    -- _mines.tools.log("Failed removing mine called '"..mine_name.."', wasn't found.")
    return {success=false, errmsg="Failed removing mine called '"..mine_name.."', wasn't found.", val=nil}
end

-- Returns ALL of the mine's data, just given the mine's name
api.info_mine = function (mine_name)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    if api.is_mine(mine_name) then
        return {success=true, errmsg="", val=m[mine_name]}
    end
    -- _mines.tools.log("Failed getting info on mine called '"..mine_name.."', wasn't found.")
    return {success=false, errmsg="Failed getting info on mine called '"..mine_name.."', wasn't found.", val=nil}
end

api.save_mine = function (mine_name, mi)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    if api.is_mine(mine_name) then
        m[mine_name] = mi
        _mines.store:set_string("mines", minetest.serialize(m))
        return {success=true, errmsg="", val=nil}
    end
    return {success=false, errmsg="Failed saving mine called '"..mine_name.."', wasn't found.", val=nil}
end

api.players_in_mine = function (mine_name)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    if not api.is_mine(mine_name) then
        return {success=false, errmsg="Failed scanning mine for players, mine called '"..mine_name.."', wasn't found.", val=nil}
    end
    local players = {}
    local mi = m[mine_name]
    local pos1 = _mines.tools.str2pos(mi.pos1)
    local pos2 = _mines.tools.str2pos(mi.pos2)
    local rc = _mines.tools.range_vecs(pos1, pos2)
    local p1 = rc.min
    local p2 = rc.max
    for i, player in ipairs(minetest.get_connected_players()) do
        local pname = player:get_player_name()
        local pos = player:get_pos()
        local player_in_mine = _mines.tools.within(p1, p2, pos)
        players[pname] = player_in_mine
    end
    return {success=true, errmsg="", val=players}
end

api.teleport_to_safety = function (mine_name)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    if not api.is_mine(mine_name) then
        return {success=false, errmsg="Failed regenerating mine called '"..mine_name.."', wasn't found.", val=nil}
    end
    local safe_players = 0
    local unsafe_players = 0
    local mi = m[mine_name]
    local pos1 = _mines.tools.str2pos(mi.pos1)
    local pos2 = _mines.tools.str2pos(mi.pos2)
    local rc = _mines.tools.range_vecs(pos1, pos2)
    local p1 = rc.min
    local p2 = rc.max
    for i, player in ipairs(minetest.get_connected_players()) do
        local pname = player:get_player_name()
        local pos = player:get_pos()
        local player_in_mine = _mines.tools.within(p1, p2, pos)
        if player_in_mine and mi.tp ~= "" then
            player:set_pos(_mines.tools.str2pos(mi.tp))
            minetest.chat_send_player(pname, mine_name .. " reset, you were teleported to safety.")
            safe_players = safe_players + 1
        elseif player_in_mine and mi.tp == "" then
            unsafe_players = unsafe_players + 1
        end
    end
    return {success=true, errmsg="", val={safe=safe_players, unsafe=unsafe_players}}
end

api.regen_mine = function (mine_name)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    if not api.is_mine(mine_name) then
        return {success=false, errmsg="Failed regenerating mine called '"..mine_name.."', wasn't found.", val=nil}
    end
    local mi = m[mine_name]
    if mi.regen_type == "time" then
        -- if its time based regeneration reset the timer
        mi.timer = mi.regen
    end
    if _mines.cache[mine_name] == nil then
        local rc = api.make_genlist(mine_name)
        if not rc.success then
            return rc
        end
        _mines.cache[mine_name] = rc.val
        _mines.tools.log("Updating cache for '"..mine_name.."'")
    end
    local rc = api.teleport_to_safety(mine_name)
    if not rc.success then
        return rc
    end
    if rc.val.unsafe ~= 0 then
        _mines.tools.log("Mine " .. mine_name .. " regenerated with " .. rc.val.unsafe .. " unsafe players still inside!")
        local owner = minetest.get_player_by_name(mi.owner) or nil
        if owner ~= nil then
            minetest.chat_send_player(mi.owner, "Mine '"..mine_name.."' regenerated with " .. rc.val.unsafe .. " unsafe players still inside!")
            minetest.chat_send_player(mi.owner, "Consider using '/mines settp " .. mine_name .. "' to set a safe teleport point for them.")
        end
    end
    local pos1 = _mines.tools.str2pos(mi.pos1)
    local pos2 = _mines.tools.str2pos(mi.pos2)
    _mines.tools.replace_in_area(nil, _mines.cache[mine_name], pos1, pos2)
    --[[rc = _mines.tools.range_vecs(pos1, pos2)
    local p1 = rc.min
    local p2 = rc.max
    local tmp = vector.new(0, 0, 0)
    for z = p1.z, p2.z, 1 do
        for y = p1.y, p2.y, 1 do
            for x = p1.x, p2.x, 1 do
                tmp.x = x
                tmp.y = y
                tmp.z = z
                minetest.swap_node(tmp, {name=_mines.cache[mine_name][math.random(#_mines.cache[mine_name])]})
            end
        end
    end]]
    return {success=true, errmsg="", val=nil}
end

api.fill_mine = function (mine_name, block)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    if not api.is_mine(mine_name) then
        return {success=false, errmsg="Failed filling mine called '"..mine_name.."', wasn't found.", val=nil}
    end
    local mi = m[mine_name]
    local rc = api.teleport_to_safety(mine_name)
    if not rc.success then
        return rc
    end
    if rc.val.unsafe ~= 0 then
        _mines.tools.log("Mine " .. mine_name .. " filled while " .. rc.val.unsafe .. " unsafe players were still inside!")
        local owner = minetest.get_player_by_name(mi.owner) or nil
        if owner ~= nil then
            minetest.chat_send_player(mi.owner, "Mine '"..mine_name.."' filled while " .. rc.val.unsafe .. " unsafe players were still inside!")
            minetest.chat_send_player(mi.owner, "Consider using '/mines settp " .. mine_name .. "' to set a safe teleport point for them.")
        end
    end

    local pos1 = _mines.tools.str2pos(mi.pos1)
    local pos2 = _mines.tools.str2pos(mi.pos2)
    _mines.tools.replace_in_area(nil, block, pos1, pos2)
    --[[local rc = _mines.tools.range_vecs(pos1, pos2)
    local p1 = rc.min
    local p2 = rc.max
    local tmp = vector.new(0, 0, 0)
    for z = p1.z, p2.z, 1 do
        for y = p1.y, p2.y, 1 do
            for x = p1.x, p2.x, 1 do
                tmp.x = x
                tmp.y = y
                tmp.z = z
                minetest.swap_node(tmp, {name=block})
            end
        end
    end]]
    return {success=true, errmsg="", val=nil}
end

-- Sets the teleport point for spawning to a public mine and used to teleport players out of a mine that's regenerating
-- Just give it the mine's name and the position
api.set_mine_tp = function (mine_name, pos)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    if api.is_mine(mine) then
        if type(pos) ~= "string" then
            m[mine_name].tp = _mines.tools.pos2str(pos)
        else
            m[mine_name].tp = pos
        end
        _mines.store:set_string("mines", minetest.serialize(m))
        return {success=true, errmsg="", val=nil}
    end
    -- _mines.tools.log("Failed setting mine teleport on mine called '"..mine_name.."', wasn't found.")
    return {success=false, errmsg="Failed setting mine teleport on mine called '"..mine_name.."', wasn't found.", val=nil}
end

-- Adds a node to a mine, given mine's name, node's itemstring and node's percent chance to be spawned in
api.add_node = function (mine_name, node, perc)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    --_mines.tools.log(minetest.serialize(m[mine_name]))
    if api.is_mine(mine_name) then
        local tmp_nodes = m[mine_name].nodes or {}
        table.insert(tmp_nodes, tostring(node).."="..tostring(_mines.tools.makePercent(perc)))
        local tmp_perc = _mines.tools.addPercent(tmp_nodes)
        if tmp_perc > 100 then
            -- _mines.tools.log("Percent exceeds 100, got "..tostring(tmp_perc).."%")
            return {success=false, errmsg="Percent exceeds 100, got "..tostring(tmp_perc).."%", val=nil}
        end
        m[mine_name].nodes = tmp_nodes
        _mines.store:set_string("mines", minetest.serialize(m))
        -- Update cache
        local rc = api.make_genlist(mine_name)
        if not rc.success then
            return rc
        end
        _mines.cache[mine_name] = rc.val
        return {success=true, errmsg="", val=nil}
    end
    -- _mines.tools.log("Failed adding a node into mine called '"..mine_name.."', wasn't found.")
    return {success=false, errmsg="Failed adding a node into mine called '"..mine_name.."', wasn't found.", val=nil}
end

-- Removes a node from a mine, given mine's name and node's itemstring
api.remove_node = function (mine_name, node)
    local m = minetest.deserialize(_mines.store:get_string("mines")) or {}
    if api.is_mine(mine_name) then
        local tmp_nodes = m[mine_name].nodes or {}
        local idx = -1
        for idex, value in ipairs(tmp_nodes) do
            tmp_val = _mines.tools.split(value, "=")
            if tmp_val[1]:lower() == node:lower() then
                idx = idex
            end
        end
        if idx ~= -1 then
            table.remove(tmp_nodes, idx)
            m[mine_name].nodes = tmp_nodes
            _mines.store:set_string("mines", minetest.serialize(m))
            -- Update cache
            local rc = api.make_genlist(mine_name)
            if not rc.success then
                return rc
            end
            _mines.cache[mine_name] = rc.val
            return {success=true, errmsg="", val=nil}
        else
            -- _mines.tools.log("Failed removing node '"..node.."' from '"..mine_name.."', as node wasn't found.")
            return {success=false, errmsg="Failed removing node '"..node.."' from '"..mine_name.."', as node wasn't found.", val=nil}
        end
    end
    -- _mines.tools.log("Failed adding a node into mine called '"..mine_name.."', wasn't found.")
    return {success=false, errmsg="Failed removing a node from mine called '"..mine_name.."', wasn't found.", val=nil}
end
