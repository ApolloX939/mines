
_mines.invalid_mine_names = {
    "help",
    "info",
    "pos1",
    "pos2",
    "create",
    "delete",
    "clear",
    "show",
    "settp",
    "settimer",
    "setpercent",
    "active",
    "inactive",
    "toggle",
    "regen",
    "reset",
    "list",
    "blocks",
    "privs",
    "moderator",
    "mod",
    "block",
    "priv",
    "moderators",
    "mods",
}

minetest.register_chatcommand("mines", {
    privs = {
        shout = true
    },
    description = "Mines regenerate terrain over time or by percent left",
    func = function (name, params)
        local parts = params:split(" ") -- Gets all parameters
        if #parts == 0 then
            _mines.cmds.help(name)
            return true
        end
        local subcmd = parts[1]:lower()
        local args = {}
        for i, v in ipairs(parts) do
            if i ~= 1 then
                table.insert(args, v)
            end
        end
        if subcmd == "help" then
            if #args == 0 then
                _mines.cmds.help(name)
                return true
            end
            local sc = args[1]:lower()
            if sc == "blocks" or sc == "block" then
                minetest.chat_send_player(name, "Mines Subcmd 'blocks' Help:")
                minetest.chat_send_player(name, "  mines blocks <mine_name>                             Lists all blocks and their percentages for the given mine")
                minetest.chat_send_player(name, "    Mines blocks Subcmds add, rm and set support the itemstring being hand in which the currently held item/node is used")
                minetest.chat_send_player(name, "  mines blocks <mine_name> add itemstring=percent      Adds a block to given mine, given itemstring and percent (minimum 0.1, maximum 100)")
                minetest.chat_send_player(name, "  mines blocks <mine_name> rm itemstring               Removes a block from the given mine, given itemstring")
                minetest.chat_send_player(name, "  mines blocks <mine_name> set itemstring=percent      Reassigns or Adds a block to the given mine, given itemstring and percent")
                minetest.chat_send_player(name, "  mines blocks <mine_name> clear                       Clears all blocks from the given mine")
                return true
            elseif sc == "privs" or sc == "priv" then
                minetest.chat_send_player(name, "Mines Subcmd 'privs' Help:")
                minetest.chat_send_player(name, "  mines privs <mine_name>                      Lists if the mine is accessable by all, and if not lists all players who have access")
                minetest.chat_send_player(name, "  mines privs <mine_name> lock                 Marks the mine as inaccessable to all, only those added to the mine can teleport to it")
                minetest.chat_send_player(name, "  mines privs <mine_name> unlock               Marks the mine as accessable to all, all players can teleport to it")
                minetest.chat_send_player(name, "  mines privs <mine_name> toggle               Toggles the mine's accessablility for if all players can teleport to it")
                minetest.chat_send_player(name, "  mines privs <mine_name> add <player_name>    Adds a player to the list of players who have access to teleport to this mine")
                minetest.chat_send_player(name, "  mines privs <mine_name> rm <player_name>     Removes a player from the list of players who have access to teleport to this mine")
                return true
            elseif sc == "moderator" or sc == "mod" or sc == "moderators" or sc == "mods" then
                minetest.chat_send_player(name, "Mines Subcmd 'moderator' or 'mod' Help:")
                minetest.chat_send_player(name, "  mines mod <mine_name>                        Lists all players who can change settings of this mine")
                minetest.chat_send_player(name, "  mines mod <mine_name> add <player_name>      Adds a player to the list of players who can change the settings of this mine")
                minetest.chat_send_player(name, "  mines mod <mine_name> rm <player_name>       Removes a player from the list of players who can change the settings of this mine")
                minetest.chat_send_player(name, "  mines mod <mine_name> clear                  Removes all players from the list of players who can change the settigns of this mine")
                minetest.chat_send_player(name, "  mines mod <mine_name> owner                  Returns who can modify the mod list settings of this mine")
                minetest.chat_send_player(name, "  mines mod <mine_name> owner <player_name>    Changes ownership of the mine, the issuer will be downgraded to a mod unless the new owner removes them from there too")
                return true
            end
        elseif subcmd == "info" then
            return _mines.cmds.info(name, args)
        elseif subcmd == "create" then
            return _mines.cmds.create(name, args)
        elseif subcmd == "pos1" then
            return _mines.cmds.pos1(name, args)
        elseif subcmd == "pos2" then
            return _mines.cmds.pos2(name, args)
        elseif subcmd == "list" then
            return _mines.cmds.list(name, args)
        elseif subcmd == "delete" then
            return _mines.cmds.delete(name, args)
        elseif subcmd == "show" then
            return _mines.cmds.show(name, args)
        elseif subcmd == "clear" then
            return _mines.cmds.clear(name, args)
        elseif subcmd == "regen" or subcmd == "reset" then
            return _mines.cmds.reset(name, args)
        elseif subcmd == "settp" then
            return _mines.cmds.settp(name, args)
        elseif subcmd == "settimer" then
            return _mines.cmds.settimer(name, args)
        elseif subcmd == "setpercent" then
            return _mines.cmds.setpercent(name, args)
        elseif subcmd == "active" then
            return _mines.cmds.active(name, args)
        elseif subcmd == "inactive" then
            return _mines.cmds.inactive(name, args)
        elseif subcmd == "toggle" then
            return _mines.cmds.toggle(name, args)
        elseif subcmd == "block" or subcmd == "blocks" then
            -- '/mines blocks 1 2 3 ...'
            local argv = {} -- Adjusts the parsing so argv[1] ('1') == args[3] ('/mines', 'blocks', '1')
            for i, v in ipairs(args) do
                if i ~= 1 or i ~= 2 then
                    table.insert(argv, v)
                end
            end
            --_mines.tools.log(minetest.serialize(argv))
            if #argv == 1 then -- If it's just '/mines blocks <mine_name>' then check if it's in this list
                if mines.is_mine(argv[1]) then
                    return _mines.cmds.block.list(name, argv)
                end
                return false, "No such mine '" .. argv[1] .. "'."
            end
            local sc = argv[2]:lower()
            if sc == "add" then
                return _mines.cmds.block.add(name, argv)
            elseif sc == "rm" then
                return _mines.cmds.block.rm(name, argv)
            elseif sc == "clear" then
                return _mines.cmds.block.clear(name, argv)
            end
        end
        -- /mines <mine_name>    teleport them
        if _mines.tools.tableContainsValue(_mines.tools.tableKeys(minetest.deserialize(_mines.store:get_string("mines")) or {}), subcmd) and not _mines.tools.tableContainsValue(_mines.invalid_mine_names, subcmd) then
            local rc = mines.info_mine(subcmd)
            if not rc.success then
                _mines.tools.log("mines " .. subcmd .. " => " .. rc.errmsg)
                return false, rc.errmsg
            end
            if rc.val.tp == "" then
                return false, "Mine '" .. subcmd .. "' has no teleport point."
            end
            if not rc.val.public and not _mines.tools.tableContainsValue(rc.val.mods, name) and not _mines.tools.tableContainsValue(rc.val.privs, name) and rc.val.owner ~= name then
                return false, "Mine '" .. subcmd .. "' is not accessable for you."
            end
            local player = minetest.get_player_by_name(name) or nil
            if player == nil then
                minetest.chat_send_player(name, "You don't exist?`")
                _mines.tools.log("mines " .. subcmd .. " => Player '" .. name .. "' doesn't exist (is nil)")
                return true
            end
            player:set_pos(_mines.tools.str2pos(rc.val.tp))
            return true, "Teleported to '" .. subcmd .. "'."
        end
        return false, "invalid command"
    end
})
