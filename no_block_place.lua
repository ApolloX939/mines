
-- Protector radius setting
local protector_radius = tonumber(minetest.settings:get("protector_radius")) or 5

minetest.register_on_placenode(function (pos, newnode, placer, oldnode, itemstack, pointed_thing)
    -- If there isn't enough info
    if pos == nil or newnode == nil then
        return true -- Block it
    end
    local mlist = minetest.deserialize(_mines.store:get_string("mines")) or {}
    for name, m in pairs(mlist) do
        if m ~= nil then
            if m.pos1 ~= "0 0 0" and m.pos2 ~= "0 0 0" then
                -- Mine has a area
                local pos1 = _mines.tools.str2pos(m.pos1)
                local pos2 = _mines.tools.str2pos(m.pos2)
                if newnode == "protector:protect" or newnode == "protector:protect2" or newnode == "protector:protect_hidden" then
                    -- If it's a protector, check protection radius for if it overlaps a mine
                    local p1 = vector.add(pos, protector_radius)
                    local p2 = vector.subtract(pos, protector_radius)
                    local r = _mines.tools.range_vecs(p1, p2)
                    p1 = r.min
                    p2 = r.max
                    local tmp = vector.new(0, 0, 0)
                    for z = p1.z, p2.z, 1 do
                        for y = p1.y, p2.y, 1 do
                            for x = p1.x, p2.x, 1 do
                                tmp.x = x
                                tmp.y = y
                                tmp.z = z
                                if _mines.tols.within(pos1, pos2, tmp) then
                                    -- The pos is within protector radius
                                    return true -- Block it
                                end
                            end
                        end
                    end
                end
                if _mines.tools.within(pos1, pos2, pos) then
                    -- Pos is within the mines area
                    return true -- Block it
                end
            end
        end
    end
end)
