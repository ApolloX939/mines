# Mines

Regenerating terrain which regenerates after a period of time or a
percent mined.

(Teleports players out if they are within it's dimensions) MT Edition

## What's in the box

* Custom API
* Chat commands
* Supports **ALL** games!
* Technic Uranium Safe ore conversion (Creates a custom node that drops the same as technics uranium ore, but without the radioactivity damage)

## Mines and protection

Mines will now work with `protector` to prevent protection of mines from other players (that is, no one can claim a mine, thus preventing anyone from mining it).

Mines will block all placement of nodes within mines.

> Currently there is an exploit using the `areas` protection mod, mines can be claimed when `areas` is used.

## Setup of a mine

> Note: All commands will require you to have the `mines` privilege (well excluding help, teleportation, info, and list)

1. Go ahead and get your area picked out for where you want your mine.
2. Then issue `/mines create <mine_name>` (Please note, mine names must be all one word, **No Spaces**, and they can't be named a `/mines` subcommand)
3. It will show a list of additional commands which can be issued to set the many things to the mine then issue `/mines active <mine_name>`
