
--[[
    TODO:
    - Need to update it so we load the map in, incase players aren't around that mine that needs to be reset (right now we do a blind update via swap)
]]

local interv = 0.0
minetest.register_globalstep(function (dtime)
    interv = interv + dtime
    if interv > 1.0 then
        local min = minetest.deserialize(_mines.store:get_string("mines")) or {}
        for mname, m in pairs(min) do
            if m.active then -- Skip over inactive mines
                if m.regen_type == "time" then
                    local tick = m.timer or (m.regen or 60)
                    tick = tick - 1
                    if tick < 10 and tick > 0 then -- less than 10 but greater than 0
                        if #minetest.get_connected_players() ~= 0 then
                            for i, player in ipairs(minetest.get_connected_players()) do
                                local pname = player:get_player_name()
                                local pos = player:get_pos()
                                local posA = _mines.tools.str2pos(m.pos1)
                                local posB = _mines.tools.str2pos(m.pos2)
                                -- Inform the player the mine is about to reset
                                local player_in_mine = _mines.tools.within(posA, posB, pos)
                                if player_in_mine then
                                    local endian = "second."
                                    if tick > 1 then
                                        endian = "seconds."
                                    end
                                    minetest.chat_send_player(pname, "[mines] " .. mname .. " will reset in " .. tostring(tick) .. " " .. endian)
                                end
                            end
                        end
                    end
                    if tick <= 0 then
                        m.timer = m.regen or 60
                        -- check for players within
                        if #minetest.get_connected_players() ~= 0 then -- Only run if someone is online
                            local rc = mines.regen_mine(mname)
                            if not rc.success then
                                _mines.tools.log("mines regen_mine time based " .. mname .. " => " .. rc.errmsg)
                                return
                            end
                            minetest.chat_send_all("Mine " .. mname .. " regenerated.")
                        end
                    else
                        m.timer = tick
                    end
                elseif m.regen_type == "percent" then
                    local pos1 = _mines.tools.str2pos(m.pos1)
                    local pos2 = _mines.tools.str2pos(m.pos2)
                    local fill = _mines.tools.filled(pos1, pos2)
                    local sizex = math.abs(pos1.x - pos2.x)
                    local sizey = math.abs(pos1.y - pos2.y)
                    local sizez = math.abs(pos1.z - pos2.z)
                    local total = sizex + sizey + sizez
                    local percent = ((fill/total) * 100.0)
                    if percent <= (m.regen or 1) then
                        if #minetest.get_connected_players() ~= 0 then -- Only run if someone is online
                            rc = mines.regen_mine(mname)
                            if not rc.success then
                                _mines.tools.log("mines regen_mine percent based " .. mname .. " => " .. rc.errmsg)
                                return
                            end
                            minetest.chat_send_all("Mine " .. mname .. " regenerated.")
                        end
                    end
                end
            end -- Skips over inactive mines
        end
        _mines.store:set_string("mines", minetest.serialize(min))
        interv = 0.0
    end
end)
