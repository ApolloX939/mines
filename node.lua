
-- Now using a glasslike node rather than liquid
-- This node is used to render a mines area (so you can get a graphical view of how big the mine is)

minetest.register_node("mines:show", {
    short_description = "Mines Show block",
    description = "Mines Show block\nThis block should not be obtainable.\nThis block is only used to depict the size of a mine.",
    drawtype = "glasslike_framed",
    is_ground_content = false,
    sunlight_propagates = true,
    walkable = false, -- You can walk thru it
    pointable = false, -- Because it's not pointable we don't need to worry about it being mined or not
    light_source = 3, -- Emit a little bit of light so we don't have some strange dark spot while viewing the mines area.
    use_texture_alpha = "blend",
    drop = "",
    -- up (+Y), down (-Y), right (+X), left (-X), back (+Z), front (-Z).
    -- (+Y, -Y, +X, -X, +Z, -Z)
    tiles = {"mines_show.png"},
    inventory_image = minetest.inventorycube("mines_show (copy 1).png"),
    wield_image = minetest.inventorycube("mines_show (copy 1).png"),
    color = {a=50, r=255, g=0, b=0},
    groups = {not_in_creative_inventory=1, oddly_breakable_by_hand=3} -- Not accessable in creative
    --groups = {oddly_breakable_by_hand=3}, -- In case we need to mine it again
})
