
-- A collection of utility functions for making lua less difficult
_mines.tools = {}
local tools = _mines.tools

-- Centralize logging
tools.log = function (input)
    minetest.log("action", "[mines] "..tostring(input))
end

-- Centralize errors
tools.error = function (input)
    error("[mines] "..tostring(input))
end

-- Returns a table of the string split by the given seperation string
tools.split = function (inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end

-- Returns true or false if search string is in inputstring
tools.contains = function(inputstr, search)
    if string.match(inputstr, search) then
        return true
    end
    return false
end

-- Returns space seperated position
tools.pos2str = function (pos)
    return "" .. tostring(math.floor(pos.x)) .. " " .. tostring(math.floor(pos.y)) .. " " .. tostring(math.floor(pos.z))
end

-- Returns a xyz vector from space seperated position
tools.str2pos = function (str)
    local pos = tools.split(str, " ")
    return vector.new(tonumber(pos[1]), tonumber(pos[2]), tonumber(pos[3]))
end

-- Returns 0, 90, 180, 270 based on direction given (assumes degree)
tools.to4dir = function (dir)
    local dir4 = math.floor(dir) / 90
    tools.log(tostring(dir4))
    if dir4 > 3.5 or dir4 < 0.5 then
        return 180
    elseif dir4 > 2.5 and dir4 < 3.5 then
        return 270
    elseif dir4 > 1.5 and dir4 < 2.5 then
        return 0
    elseif dir4 > 0.5 and dir4 < 3.5 then
        return 90
    end
    tools.log("to4dir(" .. tostring(dir) .. ") failed with " .. tostring(dir4))
    return -1
end

-- Given radians returns degrees
tools.rad2deg = function (rads)
    return (rads * 180) / 3.14159
end

-- Given degrees returns radians
tools.deg2rad = function (deg)
    return (deg * 3.14159) / 180
end

-- Converts the given string so the first letter is uppercase (Returns the converted string)
tools.firstToUpper = function (str)
    return (str:gsub("^%l", string.upper))
end

-- https://stackoverflow.com/questions/2282444/how-to-check-if-a-table-contains-an-element-in-lua
-- Checks if a value is in the given table (True if the value exists, False otherwise)
tools.tableContainsValue = function (table, element)
    for key, value in pairs(table) do
        if value == element then
            return true
        end
    end
    return false
end

tools.tableContainsKey = function (table, element)
    for key, value in pairs(table) do
        if key == element then
            return true
        end
    end
    return false
end

-- Given a table returns it's keys (Returns a table)
tools.tableKeys = function (t)
    local keys = {}
    for k, v in pairs(t) do
        table.insert(keys, k)
    end
    return keys
end

-- Given a table returns it's values (Returns a table)
tools.tableValues = function (t)
    local vals = {}
    for k, v in pairs(t) do
        table.insert(vals, v)
    end
    return vals
end

-- Returns percentage given current and max values
tools.getPercent = function (current, max)
    if max == nil then
        max = 100.0
    end
    return (current / max) * 100.0
end

-- Returns number assuming given is a percent (limit's it to within a percent 0.0-100.0)
tools.makePercent = function (value)
    if value < 0 then
        value = 0
    end
    if value > 100 then
        value = 100
    end
    return value
end

-- Attempts to calculate all the percentages then return the whole
tools.addPercent = function (values_table)
    local val = 0.0
    for k, v in pairs(values_table) do
        local v1 = tools.split(v, "=")
        val = val + tools.makePercent(tonumber(v1[#v1]))
    end
    return val
end

-- Generates the minimum and maximum vec3s given 2 vec3s
tools.range_vecs = function (pos1, pos2)
    local i = {}
    local ii = {}
    if pos1.x > pos2.x then
        i.x = pos2.x
        ii.x = pos1.x
    else
        i.x = pos1.x
        ii.x = pos2.x
    end
    if pos1.y > pos2.y then
        i.y = pos2.y
        ii.y = pos1.y
    else
        i.y = pos1.y
        ii.y = pos2.y
    end
    if pos1.z > pos2.z then
        i.z = pos2.z
        ii.z = pos1.z
    else
        i.z = pos1.z
        ii.z = pos2.z
    end
    return {min=i, max=ii}
end

-- Checks if given position is within the 2 positions
tools.within = function (pos1, pos2, pos)
    if pos == pos1 or pos == pos2 then
        return true
    end
    --_mines.tools.log("1 " .. minetest.serialize(pos1))
    --_mines.tools.log("2 " .. minetest.serialize(pos2))
    local rc = tools.range_vecs(pos1, pos2)
    local p1 = rc.min
    local p2 = rc.max
    local tmp = vector.new(0, 0, 0)
    for z = p1.z, p2.z, 1 do
        for y = p1.y, p2.y, 1 do
            for x = p1.x, p2.x, 1 do
                tmp.x = x
                tmp.y = y
                tmp.z = z
                if vector.round(pos) == tmp then
                    return true
                end
            end
        end
    end
    return false
end

tools.get_size = function (pos1, pos2)
    local rc = tools.range_vecs(pos1, pos2)
    local total = rc.max.x - rc.min.x
    total = total + rc.max.y - rc.min.y
    total = total + rc.max.z - rc.min.z
    return total
end

-- Checks how many nodes within 2 positions is not air
tools.filled = function (pos1, pos2)
    local rc = tools.range_vecs(pos1, pos2)
    local p1 = rc.min
    local p2 = rc.max
    local tmp = vector.new(0, 0, 0)
    local hits = 0
    for z = p1.z, p2.z, 1 do
        for y = p1.y, p2.y, 1 do
            for x = p1.x, p2.x, 1 do
                tmp.x = x
                tmp.y = y
                tmp.z = z
                local n = minetest.get_node_or_nil(tmp)
                if n ~= nil then
                    if n.name ~= "air" then
                        hits = hits + 1
                    end
                end
            end
        end
    end
    return hits
end

---Replaces nodes within an area, uses a Voxel Manipulator for speed.
---
---from can be nil to replace all nodes within the given pos1 to pos2
---
---to can be a table to pick a random node within to to replace the node
---@param from string
---@param to string
---@param pos1 vector
---@param pos2 vector
tools.replace_in_area = function (from, to, pos1, pos2)
    local rc = tools.range_vecs(pos1, pos2)
    local min = rc.min
    local max = rc.max

    local c_to
    local c_from
    if type(to) == "table" then
        c_to = {}
        for _, n in ipairs(to) do
            table.insert(c_to, minetest.get_content_id(n))
        end
    else
        c_to = minetest.get_content_id(to)
    end
    if from ~= nil then
        c_from = minetest.get_content_id(from)
    end

    local vm = minetest.get_voxel_manip()
    local emin, emax = vm:read_from_map(min, max)
    local a = VoxelArea:new{
        MinEdge = emin,
        MaxEdge = emax
    }
    local data = vm:get_data()

    for z = min.z, max.z do
        for y = min.y, max.y do
            for x = min.x, max.x do
                if tools.within(min, max, vector.new(x, y, z)) then
                    local vi = a:index(x, y, z)
                    if c_from ~= nil then
                        -- Replaces from with either any node within to or just to
                        if data[vi] == c_from then
                            if type(c_to) == "table" then
                                data[vi] = c_to[math.random(#c_to)]
                            else
                                data[vi] = c_to
                            end
                        end
                    else
                        -- Replaces any with either any node within to or just to
                        if type(c_to) == "table" then
                            data[vi] = c_to[math.random(#c_to)]
                        else
                            data[vi] = c_to
                        end
                    end
                end
            end
        end
    end

    vm:set_data(data)
    vm:write_to_map(true) -- Yes we want lighting to be recalculated
end
